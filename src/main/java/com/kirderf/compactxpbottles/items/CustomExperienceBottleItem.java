package com.kirderf.compactxpbottles.items;

import com.kirderf.compactxpbottles.entity.CustomExperienceBottleEntity;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class CustomExperienceBottleItem extends Item{
	public int xpmultiplyer;
	
	public CustomExperienceBottleItem(ExtraProperties properties) {
		super(properties);
		this.xpmultiplyer = properties.xpMultiplyer;
	}
	public int getXpMultiplyer(){
		 return this.xpmultiplyer;
	}


	@OnlyIn(Dist.CLIENT)
	public boolean hasEffect(ItemStack stack) {
		return true;
	}

	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		ItemStack itemstack = playerIn.getHeldItem(handIn);
		if (!playerIn.abilities.isCreativeMode) {
			itemstack.shrink(1);
		}

		worldIn.playSound((PlayerEntity) null, playerIn.posX, playerIn.posY, playerIn.posZ,
				SoundEvents.ENTITY_EXPERIENCE_BOTTLE_THROW, SoundCategory.NEUTRAL, 0.5F,
				0.4F / (random.nextFloat() * 0.4F + 0.8F));
		if (!worldIn.isRemote) {
			CustomExperienceBottleEntity experiencebottleentity = new CustomExperienceBottleEntity(worldIn, playerIn, xpmultiplyer);
			experiencebottleentity.func_213884_b(itemstack);
			experiencebottleentity.shoot(playerIn, playerIn.rotationPitch, playerIn.rotationYaw, -20.0F, 0.7F, 1.0F);
			worldIn.addEntity(experiencebottleentity);
		}

		playerIn.addStat(Stats.ITEM_USED.get(this));
		return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
	}
	public static class ExtraProperties extends Item.Properties{
		private int xpMultiplyer;


		public ExtraProperties() {
			super();
		}

		public ExtraProperties xpMultiplyer(int xpMultiplyer) {
	          this.xpMultiplyer = xpMultiplyer;
	          return this;
	       }
	}
}

