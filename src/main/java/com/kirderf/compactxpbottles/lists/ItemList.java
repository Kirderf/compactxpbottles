package com.kirderf.compactxpbottles.lists;


import com.kirderf.compactxpbottles.compactxpbottles;
import com.kirderf.compactxpbottles.items.CustomExperienceBottleItem;
import com.kirderf.compactxpbottles.items.CustomExperienceBottleItem.ExtraProperties;

import net.minecraft.item.Item;

public class ItemList {
	public static Item x4experiencebottle = new CustomExperienceBottleItem(((ExtraProperties) new CustomExperienceBottleItem.ExtraProperties().group(compactxpbottles.KirderfCreativeTab)).xpMultiplyer(4));
	public static Item x16experiencebottle = new CustomExperienceBottleItem(((ExtraProperties) new CustomExperienceBottleItem.ExtraProperties().group(compactxpbottles.KirderfCreativeTab)).xpMultiplyer(16));
	public static Item x64experiencebottle = new CustomExperienceBottleItem(((ExtraProperties) new CustomExperienceBottleItem.ExtraProperties().group(compactxpbottles.KirderfCreativeTab)).xpMultiplyer(64));
	public static Item x256experiencebottle = new CustomExperienceBottleItem(((ExtraProperties) new CustomExperienceBottleItem.ExtraProperties().group(compactxpbottles.KirderfCreativeTab)).xpMultiplyer(256));
}
